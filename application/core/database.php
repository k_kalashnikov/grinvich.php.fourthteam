<?php

$path = dirname( __FILE__) .'/';
require_once $path.'abstractDB.php';

class DataBase extends AbstractDB
{

    function DataBase($libraryPath = '', $dbType = 'mysql', $preferredResType = ASSOCIATIVE_ARRAY)
    {
        AbstractDB::AbstractDB('', 'mysql', $preferredResType);
    }


    function connect($host, $db, $user, $password)
    {
        AbstractDB::connect($host, $db, $user, $password);
        $this->db = @mysql_connect($host, $user, $password);
        if ($this->db) {
            if (@mysql_select_db($db, $this->db)) {
                return true;
            };
            $this->error = @mysql_error($this->db);
        }
        $this->error = "Error connecting to db!";
        return false;
    }

    function disconnect()
    {
        if ($this->db != null) {
            @mysql_close($this->db);
            AbstractDB::disconnect();
            return true;
        } else {
            return false;
        }
    }

    function query($query)
    {
        AbstractDB::query($query);
        if ($this->db != null) {
            $this->result = @mysql_query($query, $this->db);
            if ($this->result != false) {
                return true;
            } else {
                $this->error = @mysql_error($this->db);
                return false;
            }
        } else {
            return false;
        }
    }


    function nextResult($resultType = PREDEFINED_VALUE)
    {
        if ($resultType == PREDEFINED_VALUE) {
            $resultType = $this->prefResType;
        }
        if ($this->result != null) {
            switch ($resultType) {
                case ASSOCIATIVE_ARRAY:
                    return @mysql_fetch_array($this->result, MYSQL_ASSOC);
                    break;
                case NUMERIC_ARRAY:
                    return @mysql_fetch_array($this->result, MYSQL_NUM);
                    break;
                case BOTH:
                    return @mysql_fetch_array($this->result, MYSQL_BOTH);
                    break;
            }
        } else {
            return false;
        }
    }


    function freeResults()
    {
        if ($this->result != null) {
            return @mysql_free_result($this->result);//освобожденияе памяти
        }


        function numRows()
        {
            if ($this->result != null) {
                return @mysql_num_rows($this->result);//количество рядов результата запроса
            } else {
                return false;
            }
        }


        function affectedRows()
        {
            if ($this->db != null) {
                return @mysql_affected_rows($this->db);//число затронутых прошлой операцией рядов
            } else {
                return false;
            }
        }

        function getIdentifier()
        {
            return "NATIVE PHP";
        }

    }
}

