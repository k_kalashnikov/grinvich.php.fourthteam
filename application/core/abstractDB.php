<?php
    define("PREDEFINED_VALUE", 0);

    define("ASSOCIATIVE_ARRAY", 1);

    define("NUMERIC_ARRAY", 2);

    define("BOTH", NUMERIC_ARRAY + ASSOCIATIVE_ARRAY);

class AbstractDB {

    var $host = '';
    var $database = '';
    var $user = '';
    var $password = '';
    var $db = null;
    var $result = null;
    var $prefResType;
    var $lastQuery = '';
    var $error = '';
    var $_path;

    function AbstractDB($libraryPath, $dbType, $preferredResType = ASSOCIATIVE_ARRAY) {
        if (!@is_subclass_of($this, 'AbstractDB')) {
            trigger_error("Cannot instanciate an abstract class", E_USER_ERROR);
            return null;
        }
        $this->prefResType = $preferredResType;
        $this->_dbType = $dbType;
        $this->_path = trim($libraryPath);
        if ($this->_path != '') {
            if (substr($this->_path, -1) != "/") {
                $this->_path .= '/';
            }
        }
    }


    function connect($host, $db, $user, $password) {
        $this->host = $host;
        $this->database = $db;
        $this->user = $user;
        $this->password = $password;
        return true;
    }


    function    disconnect() {
        $this->db = null;
        $this->host = '';
        $this->database = '';
        $this->user = '';
        $this->password = '';
        return true;
    }

    function query($query) {
        $this->lastQuery = $query;
        $this->freeResults();
    }


    function execute($query, $asValue = false, $resultType = PREDEFINED_VALUE) {
        if ($this->query($query)) {
            if ($asValue) {
                return $this->allResultValues($resultType);
            } else {
                return $this->allResults($resultType);
            }
        }
        return false;
    }


    function nextResult($resultType = PREDEFINED_VALUE) {
    }


    function next($resultType = PREDEFINED_VALUE) {
        return $this->nextResult($resultType);
    }



    function allResults($resultType = PREDEFINED_VALUE) {
        if ($resultType == PREDEFINED_VALUE) {
            $resultType = $this->prefResType;
        }
        $res = array();
        while ($col = $this->nextResult($resultType)) {
            array_push($res, $col);
        }
        return $res;
    }


    function all($resultType = PREDEFINED_VALUE) {
        return $this->allResults($resultType);
    }


    function nextResultValue($resultType = PREDEFINED_VALUE) {
        if ($resultType == PREDEFINED_VALUE) {
            $resultType = $this->prefResType;
        }
        $res = $this->_fixType($this->nextResult($resultType), $resultType);
        if (is_array($res)) {
            return false;
        } else {
            return $res;
        }
    }

    function nextValue($resultType = PREDEFINED_VALUE) {
        return $this->nextResultValue($resultType);
    }

    function allResultValues($resultType = PREDEFINED_VALUE) {
        if ($resultType == PREDEFINED_VALUE) {
            $resultType = $this->prefResType;
        }
        $array = $this->allResults($resultType);
        $res = array();
        for ($i=0; $i < sizeof($array); $i++) {
            $value = $this->_fixType($array[$i], $resultType);
            if ($value == false) {
                return false;
            } else {
                $res[$i] = $value;
            }
        }
        return $res;
    }


    function allValues($resultType = PREDEFINED_VALUE) {
        return $this->allResultValues($resultType);
    }


    function freeResults() {
        return true;
    }



    function free() {
        return $this->freeResults();
    }



    function numRows() {
    }



    function affectedRows() {
    }



    function getIdentifier() {
        return "still abstract...";
    }



    function getDbType() {
        return $this->_dbType;
    }


    function _fixType($res, $resultType) {
        // too much elements?
        $temp = @array_values($res);
        $count = 1;
        if ($resultType == BOTH) {
            $count += 1;
        }
        if (sizeof($temp) == $count) {
            return $temp[0];
        } else {
            return $res;
        }
    }
}
?>
